# README #

Random bits of code used for various purposes

### What is this repository for? ###

* Saving bits of code I've used at various stages, usually for no good reason
* Version 1.0.0

### How do I get set up? ###

* You don't need anything but the files and PHP 5.6 or newer

### Contribution guidelines ###

* Unlikely to be a problem

### Who do I talk to? ###

* gary@praterraines.co.uk