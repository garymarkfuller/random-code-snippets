<?php

/* 
 * Outputs the nth number of the Fibonacci sequence
 * Pass in $n during the function call to return that number in the sequence
 */

function getFibonacci ($n) 
{
	$x = 0;
	$y = 1;
	for ($i = 3; $i <= $n; $i++) {
		$z = $x + $y;	
		$x = $y;
		$y = $z;
	}
	return $y;
}
$c = getFibonacci (24);
echo $c;