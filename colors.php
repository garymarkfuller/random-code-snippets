<?php
echo '<h1>256 Black - White</h1>';
for ($r = 0, $g = 0, $b = 0; $r <= 255, $g <= 255, $b <= 255; $r++, $g++, $b++) {
    echo "<div style='display: block; width: 20px; height: 20px; padding: 0; margin: 1px; float: left; background: rgb($r,$g,$b);'></div>";
}
echo '<h1>16777216 Black - White</h1>';
for ($r = 0; $r <= 255; $r++) {
    for ($g = 0; $g <= 255; $g++) {
        for ($b = 0; $b <= 255; $b++) {
            echo "<div style='display: block; width: 20px; height: 20px; padding: 0; margin: 1px; float: left; background: rgb($r,$g,$b);'></div>";
        }
    }    
}
echo '<h1>256 variations around #FA9D24</h1>';
for ($r = 0; $r <= 255; $r++) {
    echo "<div style='display: block; width: 20px; height: 20px; padding: 0; margin: 1px; float: left; background: rgb(250,$r,36);'></div>";
}