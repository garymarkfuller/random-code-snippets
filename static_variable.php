<?php

/* 
 * Demonstrates the purpose of a static variable in a function
 */

  function staticFunction() {
   static $count = 0;
   $count++;
   echo "Function has executed $count time(s)< br/>";
  }

  for($i = 0; $i < 5; $i++) {
   staticFunction();
  }