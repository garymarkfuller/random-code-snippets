<?php
/** Demonstrating the structure of a call to google calendar to create a new calendar item. **/
$example="http://www.google.com/calendar/render?action=TEMPLATE&text=Train+to+London%20Charing%20Cross+|+thetrainline.com&dates=20150713T160000Z/20150713T173200Z&location=Departing+from+Folkestone%20Central+at+17:00&trp=false&sprop&sprop=name:&sf=true&output=xml&details=Details+for+your+journey+on+13%20July%202015.%0A%0ADeparting+from+Folkestone%20Central+at+17:00.%0AArriving+into+London%20Charing%20Cross+at+18:32.%0A%0AFor+full+journey+details+please+log+in+to+my+account+at+www.thetrainline.com.";
echo "<p><a href='$example'>Example</a></p>";

/** Replacing some elements of the structure of a call to google calendar to create a new calendar item. **/
$text = 'A+fine+day';
$dates = '20151225T120000Z/20151225T200000Z';
$location = 'CT20+3BY';
$details = 'This+is+a+test';
$url = "http://www.google.com/calendar/render?action=TEMPLATE&text=$text&dates=$dates&location=$location&trp=false&sprop&sprop=name:&sf=true&output=xml&details=$details";
echo "<a href='$url'>Basic Replacement</a>";

/** Using a function to beging to produce the structure of a call to google calendar to create a new calendar item. **/
function executeGoogleCalendarLink($event_name, $event_start, $event_end, $event_location, $event_details) 
        {

        $date_reg = ['-', ':', '+0100'];  
        $tag_array = ['</p>', '</h2>', '</h3>', '</h4>', '</h5>', '</h1>', '</li>']; 
        $google_url = "http://www.google.com/calendar/render?action=TEMPLATE&text="; 
        $google_title = str_replace(' ', '+', $event_name);
        $google_dates = "&dates=" . str_replace($date_reg, '', $event_start) . "/"; 
        $google_dates .= str_replace($date_reg, '', $event_end) . "&ctz=Europe/London";
        $google_location = "&location=" . str_replace(' ', '+', $event_location);
        $google_details = "&details=" . strip_tags(str_replace($tag_array, '%0A%0A', str_replace(' ', '+', $event_details)));

        $final_google_url = $google_url . $google_title . $google_dates . $google_location . $google_details;

        return $final_google_url;
        }

$proper_url = executeGoogleCalendarLink('My Birthday', '2015-12-27T07:30:00', '2015-12-27T23:30:00', 'My local', 'I\'m boring so there won\'t be much going on');
echo "<a href='$proper_url'>Better Function Based Replacement</a>";