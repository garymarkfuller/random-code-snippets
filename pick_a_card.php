<?php
$deck = array(
'Hearts' => array('Ace',2,3,4,5,6,7,8,9,'Jack','Queen','King'),
'Spades' => array('Ace',2,3,4,5,6,7,8,9,'Jack','Queen','King'),
'Diamonds' => array('Ace',2,3,4,5,6,7,8,9,'Jack','Queen','King'),
'Clubs' => array('Ace',2,3,4,5,6,7,8,9,'Jack','Queen','King'),
);
$suit_name = array_rand($deck);
$suit_cards = $deck[$suit_name];
$card_list = array_rand($suit_cards);
$card = $suit_cards[$card_list];
echo '<p>' . $card . ' of ' . $suit_name . '.</p>';

foreach ($deck as $suit => $cards) {
	echo '<p>' . $suit . '</p><ul>';
    foreach ($cards as $individual_card) {
		echo '<li>'. $individual_card . '</li>';
    }
	echo '</ul>';
}